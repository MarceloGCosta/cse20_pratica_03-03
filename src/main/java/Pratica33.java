import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * @author Marcelo G. Costa <marcelo@unicode.com.br>
 */
public class Pratica33 {

    public static void main(String[] args) {
        Matriz orig = new Matriz(3, 2);
        double[][] m = orig.getMatriz();
        m[0][0] = 0.0;
        m[0][1] = 0.1;
        m[1][0] = 1.0;
        m[1][1] = 1.1;
        m[2][0] = 2.0;
        m[2][1] = 2.1;
        Matriz transp = orig.getTransposta();
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz transposta: " + transp);
        
        Matriz m1 = new Matriz(3,3);
        m = m1.getMatriz();
        m[0][0] = 3.0;
        m[0][1] = 0.0;
        m[0][2] = 5.0;
        m[1][0] = 2.0;
        m[1][1] = 1.0;
        m[1][2] = 0.0;
        m[2][0] = 0.0;
        m[2][1] = 2.0;
        m[2][2] = 3.0;
        
        Matriz m2 = new Matriz(3,3);
        m = m2.getMatriz();
        m[0][0] = 4.0;
        m[0][1] = 2.0;
        m[0][2] = 1.0;
        m[1][0] = 4.0;
        m[1][1] = 0.0;
        m[1][2] = 17.0;
        m[2][0] = 2.0;
        m[2][1] = 3.0;
        m[2][2] = 0.0;
        
        System.out.println("Matriz 1:\n " + m1);
        System.out.println("Matriz 2:\n " + m2);
        
        System.out.println("m1 + m2:\n " + m1.soma(m2));
        System.out.println("m1 x m2:\n " + m1.prod(m2));
        System.out.println("m2 x m1:\n " + m2.prod(m1));
    }
}
