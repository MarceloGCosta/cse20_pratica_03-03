package utfpr.ct.dainf.if62c.pratica;

/**
 * Representa uma matriz de valores {@code double}.
 * @author Wilson Horstmeyer Bogadao <wilson@utfpr.edu.br>
 */
public class Matriz {
    
    // a matriz representada por esta classe
    private final double[][] mat;
    
    /**
     * Construtor que aloca a matriz.
     * @param m O número de linhas da matriz.
     * @param n O número de colunas da matriz.
     */
    public Matriz(int m, int n) {
        mat = new double[m][n];
    }
    
    /**
     * Retorna a matriz representada por esta classe.
     * @return A matriz representada por esta classe
     */
    public double[][] getMatriz() {
        return mat;
    }
    
    /**
     * Retorna a matriz transposta.
     * @return A matriz transposta.
     */
    public Matriz getTransposta() {
        Matriz t = new Matriz(mat[0].length, mat.length);
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                t.mat[j][i] = mat[i][j];
            }
        }
        return t;
    }
    
    /**
     * Retorna a soma desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser somada
     * @return A soma das matrizes
     */
    public Matriz soma(Matriz m) {
        //throw new UnsupportedOperationException("Soma de matrizes não implementada.");
        
        Matriz soma = new Matriz(mat.length,mat[0].length);
        
        for(int i=0; i<mat.length; i++){
            for(int i2=0; i2<mat.length; i2++){
                soma.mat[i][i2] = this.mat[i][i2] + m.mat[i][i2];
            }
        }
        
        return soma;
    }

    /**
     * Retorna o produto desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser multiplicada
     * @return O produto das matrizes
     */
    public Matriz prod(Matriz m) {
        //throw new UnsupportedOperationException("Produto de matrizes não implementado.");
        
        //numero de linhas da primeira matriz e numero de colunas da segunda matriz
        Matriz produto = new Matriz(this.mat.length, m.mat[0].length);
        
        //presumindo q o numero de colunas da primeira matriz é igual ao de linhas da segunda
        //roda por cada linha de produto
        //roda por cada coluna de produto
        //roda multiplicando os elementos da linha e da coluna encontrada
        double temp;
        for(int i=0; i<produto.mat.length; i++){
            for(int i2=0; i2<produto.mat[0].length; i2++){
                temp=0;
                
                for(int i3=0; i3<m.mat.length; i3++){
                    temp += this.mat[i][i3] * m.mat[i3][i2];
                }
                
                produto.mat[i][i2]=temp;
            }
        }
        
        return produto;
    }

    /**
     * Retorna uma representação textual da matriz.
     * Este método não deve ser usado com matrizes muito grandes
     * pois não gerencia adequadamente o tamanho do string e
     * poderia provocar um uso excessivo de recursos.
     * @return Uma representação textual da matriz.
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (double[] linha: mat) {
            s.append("[ ");
            for (double x: linha) {
                s.append(x).append(" ");
            }
            s.append("]");
        }
        return s.toString();
    }
    
}
